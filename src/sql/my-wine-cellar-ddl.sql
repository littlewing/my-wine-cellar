CREATE DATABASE "my-wine-cellar"
WITH OWNER = postgres
ENCODING = 'UTF8'
TABLESPACE = pg_default
LC_COLLATE = 'French_France.1252'
LC_CTYPE = 'French_France.1252'
CONNECTION LIMIT = - 1;
COMMENT ON DATABASE "my-wine-cellar" IS ' ';


-- Table: user_wine

-- DROP TABLE user_wine;

CREATE TABLE user_wine
(
  id       SERIAL NOT NULL,
  username TEXT,
  token    TEXT,
  CONSTRAINT user_wine_pkey PRIMARY KEY (id)
)
WITH (
  OIDS = FALSE
);
ALTER TABLE user_wine OWNER TO postgres;


CREATE TABLE PARTY (
  id           SERIAL       NOT NULL PRIMARY KEY,
  name         VARCHAR(200) NOT NULL,
  user_wine_id INT REFERENCES USER_WINE (id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE PRODUCER (
  address VARCHAR(255),
  zip     VARCHAR(255),
  city    VARCHAR(255),
  country VARCHAR(255),
  phone   VARCHAR(255),
  domain  VARCHAR(255),
  region  VARCHAR(255),
  PRIMARY KEY (id)
)
  INHERITS (PARTY);

CREATE TABLE TAG (
  weight INT,
  PRIMARY KEY (id)
)
  INHERITS (PARTY);

CREATE TABLE BOTTLE (
  producer_id_seq INT REFERENCES PRODUCER (id) ON UPDATE CASCADE ON DELETE SET NULL,
  quantity        INT,
  todrink         DATE,
  acquired        DATE,
  denomination    VARCHAR(255),
  vintage         INT,
  PRIMARY KEY (id)
)
  INHERITS (PARTY);

CREATE TABLE BOTTLE_TAG (
  bottle_id_seq INT REFERENCES BOTTLE (id) ON UPDATE CASCADE ON DELETE CASCADE,
  tag_id_seq    INT REFERENCES TAG (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE TASTING (
  tastingdate   DATE NOT NULL,
  observation   VARCHAR(256),
  rate          SMALLINT,
  quantity      INT,
  bottle_id_seq INT REFERENCES BOTTLE (id) ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY (id)
)
  INHERITS (PARTY);

