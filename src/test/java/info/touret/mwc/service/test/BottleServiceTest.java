package info.touret.mwc.service.test;

import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.Producer;
import info.touret.mwc.model.Tag;
import info.touret.mwc.service.BottleService;
import info.touret.mwc.service.ProducerService;
import info.touret.mwc.service.TagService;
import info.touret.mwc.service.finder.LikeFinderBySelectedAttrs;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * User: touret-a
 * Date: 07/09/12
 * Time: 08:31
 */
@RunWith(MockitoJUnitRunner.class)
public class BottleServiceTest {


    @Mock
    EntityManager em;

    @Mock
    LikeFinderBySelectedAttrs<Bottle> likeFinder;

    @Mock
    TagService tagService;

    @Mock
    ProducerService producerService;

    @InjectMocks
    BottleService bottleService = new BottleService();

    private Bottle bottle;
    private Producer producer;

    private static final Logger trace = Logger.getAnonymousLogger();

    @Before
    public void setUp() throws Exception {
        bottle = new Bottle();

        producer = new Producer();
        producer.setName("TEST MOCKITO");
        producer.setId(1);
        bottle.setName("TESTMOCKITO");
        bottle.setProducer(producer);

        List<Tag> tags = new ArrayList<Tag>();
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("tag1");
        tags.add(tag);
        bottle.setTags(tags);
        List<Bottle> bottleList = new ArrayList<Bottle>();
        bottleList.add(bottle);
        when(likeFinder.findFullText(em, Bottle.class, bottle, "TEST", "name")).thenReturn(bottleList);
    }


    @Test
    public void testSave() throws Exception {
        bottleService.save(bottle);
        assertNotNull(bottle.getId());
    }

    @Test
    public void testFindFullText() throws Exception {
        List<Bottle> bottles = bottleService.findLikeFullText(Bottle.class, bottle, "TEST", "name");
        trace.info("bottles =" + bottles);
        assertNotNull(bottles);
        assertEquals(bottles.size(), 1);

    }
}
