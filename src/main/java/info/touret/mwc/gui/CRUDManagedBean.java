package info.touret.mwc.gui;

import info.touret.mwc.model.Identifier;
import info.touret.mwc.service.BusinessService;
import info.touret.mwc.service.ServiceException;
import org.primefaces.event.RowEditEvent;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 09/12/11
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
public abstract class CRUDManagedBean<T extends Identifier> implements Serializable {

    protected List<T> entities;

    private static final Logger trace = Logger.getLogger(CRUDManagedBean.class.getCanonicalName());

    protected T entityTableSelection;

    /**
     * retourne la selection (ligne) d un tableau
     */
    public T getEntityTableSelection() {
        return entityTableSelection;
    }

    public void setEntityTableSelection(T entityTableSelection) {
        this.entityTableSelection = entityTableSelection;
    }

    /**
     * Retourn une entite ( utilise dans l insertion)
     *
     * @return l entite
     */
    public T getEntity() {
        if (entity == null) {
            try {
                setEntity(getDomainClass().newInstance());
            } catch (InstantiationException e) {
                trace.severe(e.getMessage());
            } catch (IllegalAccessException e) {
                trace.severe(e.getMessage());
            }
        }
        return entity;
    }

    /**
     * Retourne une liste d entite
     *
     * @return la liste non nulle
     */
    public List<T> getEntities() {
        if (entities == null) {
            filterEntities();
        }
        return entities;
    }

    /**
     * Initialise la liste des entites dans le contexte
     *
     * @param entities
     */
    public void setEntities(List<T> entities) {
        this.entities = entities;
    }

    /**
     * Initialise l entite courante
     */
    public void setEntity(T entity) {
        this.entity = entity;
    }

    protected T entity;

    /**
     * Sauve une nouvelle entite
     *
     * @param event : evenement JSF
     * @throws ServiceException L'exception a echouée
     */
    public void saveNew(ActionEvent event) {
        try {
            getService().save(getEntity());
            FacesContext.getCurrentInstance().addMessage("null", new FacesMessage(FacesMessage.SEVERITY_INFO, getEntityDescription() + " a été inséré!", getEntityDescription() + " a été inséré!"));
            setEntities(null);
            /* TODO a voir si garde la mise en cache des entites ou non */
            setCompleteListOfEntities(null);
            resetNew();
        } catch (ServiceException e) {
            FacesContext.getCurrentInstance().addMessage("null", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur lors de la suppression de " + getEntityDescription(), ""));
            throw e;
        }
    }

    /**
     * Supprime une entite
     *
     * @param _entity l entite a supprimer
     * @throws ServiceException : une exception a eu lieu dans la couche service
     */
    public void delete(T _entity) {
        try {
            getService().delete(_entity);
            setEntities(null);
            setCompleteListOfEntities(null);
        } catch (ServiceException e) {
            FacesContext.getCurrentInstance().addMessage("null", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur lors de la suppression de " + getEntityDescription(), ""));
        }
    }

    /*
    * Contrainte CDI vs JSF
    * */
    public void preRenderView() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(Boolean.TRUE);
    }

    /**
     * Methode generique de mise a jour d une ligne
     *
     * @param event evenement d edition
     */
    public void updateCurrent(RowEditEvent event) {
        try {
            T entityToUpdate = (T) event.getObject();
            getService().update(entityToUpdate);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("null", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur lors de la mise à jour" + getEntityDescription(), ""));
        }
    }

    protected abstract T newInstance();

    protected abstract String url();

    /**
     * Réinitialise l instance courante
     *
     * @return url de redirection avec le parametre <code>faces-redirect=true</code>
     */
    public String resetNew() {
        setEntity(newInstance());
        return url().concat("?faces-redirect=true");
    }

    protected abstract String getEntityDescription();


    protected abstract BusinessService<T> getService();

    protected abstract Class<T> getDomainClass();


    protected String filter;

    public String getFilter() {
        if (filter == null) {
            setFilter(new String(""));
        }
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * Retourne la liste des attributs du pojo a prendre en compte dans le filtre
     *
     * @return la liste des attributs ( ex. name,denomination)
     */
    protected abstract String[] getFilterColumns();

    /**
     * Retourne une instance par defaut du pojo a prendre en compte dans le tableau
     *
     * @return l instance
     */
    protected abstract T getDefaultDomainObject();


    protected List<T> completeListOfEntities;

    public List<T> getCompleteListOfEntities() {
        if (completeListOfEntities == null) {
            setCompleteListOfEntities(getService().list(getDomainClass()));
        }
        return completeListOfEntities;
    }

    public void setCompleteListOfEntities(List<T> completeListOfEntities) {
        this.completeListOfEntities = completeListOfEntities;
    }

    protected List<T> filteredEntities;

    public List<T> getFilteredEntities() {
        return filteredEntities;
    }

    public void setFilteredEntities(List<T> filteredEntities) {
        this.filteredEntities = filteredEntities;
    }

    /**
     * listener pour filtrer les entites
     */
    public void filterEntities() {
        if (!"".equals(getFilter())) {
            setFilteredEntities(getService().findLikeFullText(getDomainClass(), getDefaultDomainObject(), getFilter(), getFilterColumns()));
            setEntities(getFilteredEntities());
        } else {
            setEntities(getCompleteListOfEntities());
        }
    }
}
