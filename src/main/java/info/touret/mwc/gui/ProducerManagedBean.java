/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui;

import info.touret.mwc.model.Producer;
import info.touret.mwc.service.BusinessService;
import info.touret.mwc.service.ProducerService;
import info.touret.mwc.service.geocoding.MapService;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import java.util.logging.Logger;


/**
 * @author touret-a
 */
@Named("producerManagedBean")
@RequestScoped
public class ProducerManagedBean extends CRUDManagedBean<Producer> {

    /**
     * Creates a new instance of ProducerManagedBean
     */
    public ProducerManagedBean() {
    }

    static final Logger trace = Logger.getLogger(ProducerManagedBean.class.getName());


    protected String getEntityDescription() {
        return new StringBuffer("Le producteur " + getEntity().getName()).toString();
    }

    @Override
    protected Producer newInstance() {
        return new Producer();
    }

    @Override
    protected String url() {
        return "/faces/producermgmt";
    }

    @EJB
    ProducerService service;

    @Override
    protected BusinessService<Producer> getService() {
        return service;
    }

    @Override
    protected Class<Producer> getDomainClass() {
        return Producer.class;
    }

    @EJB
    MapService mapService;


    public void onAdressUpdate(ActionEvent actionEvent) {
        mapModel = new DefaultMapModel();
        setEntity(mapService.geolocalize(getEntity()));
        mapModel.addOverlay(new Marker(new LatLng(getEntity().getLattitude(), getEntity().getLongitude()), getEntity().getDomain().concat("/").concat(getEntity().getName())));
    }

    private MapModel mapModel;

    public MapModel getMapModel() {
        if (mapModel == null) {
            mapModel = new DefaultMapModel();
            for (Producer producer : getEntities()) {
                mapModel.addOverlay(new Marker(new LatLng(producer.getLattitude(), producer.getLongitude()), producer.getDomain().concat("/").concat(producer.getName())));
            }
        }
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    @Override
    protected String[] getFilterColumns() {
        return new String[]{"name", "zip", "city", "country"};
    }

    @Override
    protected Producer getDefaultDomainObject() {
        return new Producer();
    }
}
