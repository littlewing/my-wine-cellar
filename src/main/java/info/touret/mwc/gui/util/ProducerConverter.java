/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui.util;


import info.touret.mwc.service.ProducerService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Logger;

/**
 * @author touret-a
 */
@Deprecated
@Named("producerConverter")
public class ProducerConverter implements Converter {

    @Inject
    ProducerService service;

    private static Logger trace = Logger.getLogger(ProducerConverter.class.getCanonicalName());

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String id) {
        try {
            //return service.findByID(Producer.class, Integer.parseInt(id));
            return new Integer(id);
        } catch (Exception e) {
            throw new ConverterException(e);
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String asString = null;
        if (o != null) {
            asString = String.valueOf(o);
        }
        return asString;
    }
}
