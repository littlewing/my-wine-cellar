/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui.util;

import info.touret.mwc.model.Bottle;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;

import java.util.logging.Logger;

/**
 * @author touret-a
 */
public class ScheduleEventWrapper {

    private Bottle bottle;

    public ScheduleEventWrapper(Bottle _bottle) {
        setBottle(_bottle);
    }

    public ScheduleEventWrapper() {
    }

    public Bottle getBottle() {
        return bottle;
    }

    public void setBottle(Bottle bottle) {
        this.bottle = bottle;
    }

    public ScheduleEvent getScheduleEvent() {
        ScheduleEvent se = null;
        if (bottle != null) {
            se = new DefaultScheduleEvent(bottle.getName() + "-"
                    + bottle.getProducer().getName(), bottle.getTodrink(),
                    bottle.getTodrink(), true);
            se.setId(String.valueOf(bottle.getId()));
            trace.severe("EVENT =" + se.getId());
        }
        return se;
    }

    private static final Logger trace = Logger
            .getLogger(ScheduleEventWrapper.class.getName());
}
