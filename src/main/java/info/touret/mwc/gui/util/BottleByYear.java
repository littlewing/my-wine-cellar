/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package info.touret.mwc.gui.util;

/**
 * @author touret-a
 */
public class BottleByYear {

    private Integer vintage;
    private Integer number;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getVintage() {
        return vintage;
    }

    public void setVintage(Integer vintage) {
        this.vintage = vintage;
    }

    public BottleByYear(Integer vintage, Integer number) {
        this.vintage = vintage;
        this.number = number;
    }

    public BottleByYear(Integer vintage) {
        this.vintage = vintage;
        this.number = 1;
    }

    @Override
    public String toString() {
        return this.vintage + "->" + this.number;
    }


}
