package info.touret.mwc.gui.util;

import javax.enterprise.context.NormalScope;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * User: touret-a
 * Date: 05/09/12
 * Time: 17:01
 */
@Target(value = {METHOD, TYPE, FIELD})
@Retention(value = RUNTIME)
@NormalScope
@Inherited
public @interface ViewScoped {
}
