package info.touret.mwc.gui.util;

import info.touret.mwc.model.Tasting;

import javax.inject.Named;
import java.io.Serializable;

/**
 * User: touret-a
 * Date: 06/09/12
 * Time: 10:18
 */
@Named("selection")
@ViewScoped
public class FormSelection implements Serializable {
    private Tasting selectedTasting;

    public Tasting getSelectedTasting() {
        return selectedTasting;
    }

    public void setSelectedTasting(Tasting selectedTasting) {
        this.selectedTasting = selectedTasting;
    }

}
