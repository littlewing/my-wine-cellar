package info.touret.mwc.gui.util;

import info.touret.mwc.model.Tag;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 14/12/11
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */
@Named("tagConverter")
public class TagsConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        List<Tag> tags = new ArrayList<Tag>();
        try {
            if (s != null && !s.isEmpty()) {
                if (!s.contains(",")) {
                    Tag tag = new Tag();
                    tag.setName(s);
                    tags.add(tag);
                } else {
                    StringTokenizer tokenizer = new StringTokenizer(s, ",");
                    while (tokenizer.hasMoreTokens()) {
                        Tag tag = new Tag();
                        tag.setName(tokenizer.nextToken());
                        tags.add(tag);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            throw new ConverterException(e);
        }
        return tags;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        String asString = "";
        if (o != null && o instanceof List) {
            List<Tag> tags = (List<Tag>) o;
            for (Tag tag : tags) {
                asString = asString.concat(tag.getName()).concat(",");
            }
        }
        return asString;
    }

    public TagsConverter() {
    }
}
