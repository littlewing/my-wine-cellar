package info.touret.mwc.gui.util;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;


/**
 * User: touret-a
 * Date: 05/09/12
 * Time: 17:06
 */
public class ViewContextExtension implements Extension {
    public void afterBeanDiscovery(@Observes AfterBeanDiscovery event, BeanManager manager) {
        event.addContext(new ViewContext());
    }

}
