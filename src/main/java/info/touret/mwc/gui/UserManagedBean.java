/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui;


import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;
import info.touret.mwc.util.Logged;
import info.touret.mwc.util.UserWineAccount;
import info.touret.mwc.util.UserWineNotFoundException;
import info.touret.mwc.util.UserWineWrapper;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Logger;

/**
 * Managed bean gerant l identification
 *
 * @author touret-a
 */
@Named("userManagedBean")
@SessionScoped
public class UserManagedBean implements Serializable {

    @Inject
    private ApplicationManagedBean applicationManagedBean;

    public UserManagedBean() {
    }

    @Inject
    UserWineWrapper userWineWrapper;

    private UserWine user;

    public UserWine getUser() {
        if (user == null) {
            setUser(createUserWine());
        }
        return user;
    }

    /**
     * gere le retour a la page d identification
     */
    private void goToLogin() {
        try {
            ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).sendRedirect(applicationManagedBean.getBaseUrl().concat("/faces/login.xhtml"));
        } catch (IOException e) {
        }
    }


    /**
     * Cree un utilisateur de type UserWine a partir du retour de l identification google et le met en cache
     *
     * @return l utilisateur
     */
    @Logged
    @Produces
    public UserWine createUserWine() {
        UserWine userWine = null;
        if (user == null) {
            userWine = new UserWine();
            trace.fine("Creation instance user");
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            if (session == null) {
                goToLogin();
            }
            UserWineAccount userWineAccount = (UserWineAccount) session.getAttribute("login_account");
            trace.info("Utilisateur retourne par GIT :" + userWineAccount);
            if (userWineAccount == null) {
                // erreur, la session n existe pas ! retour identification
                goToLogin();
            }
            try {
                userWine = userWineWrapper.setAccount(userWineAccount).toUserWine();
            } catch (UserWineNotFoundException e) {
                goToLogin();
            }
            setUser(userWine);
        } else {
            userWine = user;
        }
        trace.fine("USER GENERE PAR CDI : " + userWine);
        return userWine;
    }

    @Inject
    private BusinessService<UserWine> service;

    private final static Logger trace = Logger.getLogger(UserManagedBean.class.getName());

    /**
     * Initialise les utilisateurs
     *
     * @param _user
     */
    public void setUser(UserWine _user) {
        this.user = _user;
    }

    /**
     * retourne la requete d autorisation pour la gestion du calendrier google
     *
     * @return la locale
     */
    public Locale getCurrentLocale() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
    }

    /**
     * Retourne le message
     *
     * @param severity
     * @param shortMessage
     * @param longMessage
     * @param messageParams
     * @return
     */
    public FacesMessage getMessage(FacesMessage.Severity severity, String shortMessage, String longMessage, Object... messageParams) {
        return new FacesMessage(severity,
                getMessage(shortMessage, getCurrentLocale(), messageParams),
                getMessage(longMessage, getCurrentLocale(), messageParams));
    }


    /**
     * retourne un message donne
     *
     * @param message
     * @param locale
     * @param params
     */
    public String getMessage(String message, Locale locale, Object... params) {
        String newmessage = "";
        ResourceBundle bundle = ResourceBundle.getBundle("messages", locale);
        Set<String> keys = bundle.keySet();
        message = bundle.getString(message);
        int i = 0;
        for (String current : keys) {
            newmessage = newmessage.replace(current, params[i++].toString());
            if (i >= params.length) {
                break;
            }
        }
        return newmessage;
    }

    /**
     * Retourne le minimum
     */
    public Integer getMinBottleYear() {
        Calendar cal = GregorianCalendar.getInstance(getCurrentLocale());
        return cal.get(Calendar.YEAR) - 30;
    }

    /**
     * Retourne le max
     */
    public Integer getMaxBottleYear() {
        Calendar cal = GregorianCalendar.getInstance(getCurrentLocale());
        return cal.get(Calendar.YEAR);
    }

    public void logout(ActionEvent event) {
        ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session = null;
    }

}
