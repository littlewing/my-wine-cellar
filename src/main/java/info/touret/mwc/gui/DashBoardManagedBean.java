/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui;

import com.google.common.base.Function;
import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.DateInterval;
import info.touret.mwc.model.Tag;
import info.touret.mwc.model.Tasting;
import info.touret.mwc.service.BottleService;
import info.touret.mwc.service.TagService;
import info.touret.mwc.service.TastingService;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.model.chart.PieChartModel;
import org.primefaces.model.tagcloud.DefaultTagCloudItem;
import org.primefaces.model.tagcloud.DefaultTagCloudModel;
import org.primefaces.model.tagcloud.TagCloudModel;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author touret-a
 */
@Named("dashboardManagedBean")
@RequestScoped
public class DashBoardManagedBean {

    private DashboardModel boardModel;

    private final static Logger trace = Logger.getLogger(DashBoardManagedBean.class.getCanonicalName());

    public DashboardModel getBoardModel() {
        if (boardModel == null) {
            setBoardModel(new DefaultDashboardModel());
            DashboardColumn column1 = new DefaultDashboardColumn();
            DashboardColumn column2 = new DefaultDashboardColumn();
            column1.addWidget("aide");
            column1.addWidget("calendar");
            column2.addWidget("yearchart");
            column2.addWidget("tagchart");
            boardModel.addColumn(column1);
            boardModel.addColumn(column2);
        }
        return boardModel;
    }

    public void setBoardModel(DashboardModel boardModel) {
        this.boardModel = boardModel;
    }

    @EJB
    private BottleService bottleService;
    private List<Bottle> bottles;
    @EJB
    private TagService tagService;

    @EJB
    private TastingService tastingService;

    public List<Bottle> getBottles() {
        if (bottles == null) {
            setBottles(bottleService.getBottleList());
        }
        return bottles;
    }

    public void setBottles(List<Bottle> bottles) {
        this.bottles = bottles;
    }


    public DashBoardManagedBean() {
        super();
    }

    private List<Bottle> nextBottlesToDrink;

    public List<Bottle> getNextBottlesToDrink() {
        if (nextBottlesToDrink == null) {
            setNextBottlesToDrink(bottleService.findBottlesDrunkByDate(DateInterval.getNextMonths(6)));
        }
        return nextBottlesToDrink;
    }

    public void setNextBottlesToDrink(List<Bottle> nextBottlesToDrink) {
        this.nextBottlesToDrink = nextBottlesToDrink;
    }

    private PieChartModel bottleCountByYear;

    public PieChartModel getBottleCountByYear() {
        if (bottleCountByYear == null) {
            Map<String, Number> bottleMap = new HashMap<String, Number>(getBottles().size());
            for (Bottle current : getBottles()) {
                if (bottleMap.get(current.getVintage()) == null) {
                    bottleMap.put(String.valueOf(current.getVintage()), current.getQuantity());
                } else {
                    Number by = bottleMap.get(String.valueOf(current.getVintage()));
                    by = by.intValue() + current.getQuantity();
                    bottleMap.put(String.valueOf(current.getVintage()), by);
                }

            }
            setBottleCountByYear(new PieChartModel(bottleMap));
        }
        return bottleCountByYear;
    }

    public void setBottleCountByYear(PieChartModel _bottleCountByYear) {
        this.bottleCountByYear = _bottleCountByYear;
    }


    public DonutChartModel getAllTagsModel() {
        DonutChartModel model = new DonutChartModel();
        Map<String, Number> circle = new HashMap();
        Map<String, Tag> tags = tagService.getIndexedTags();
        for (String current : tags.keySet()) {
            circle.put(current, tags.get(current).getWeight());
        }

        model.addCircle(circle);
        return model;
    }

    public TagCloudModel getTagCloudModel() {
        TagCloudModel model = new DefaultTagCloudModel();
        Map<String, Tag> tags = tagService.getIndexedTags();
        for (String current : tags.keySet()) {
            model.addTag(new DefaultTagCloudItem(current, "#", getTagStrength(tags.get(current))));
        }
        model.addTag(new DefaultTagCloudItem("TESTCLOUD", 110));
        return model;
    }

    private Integer getTagStrength(Tag tag) {
        Function<Tag, Integer> tagFunction = new Function<Tag, Integer>() {
            @Override
            public Integer apply(Tag input) {
                Integer strength = new Integer(1);
                if (input.getWeight() >= 2) {
                    strength = 2;
                } else {
                    if (input.getWeight() >= 5) {
                        strength = 3;
                    } else {
                        if (input.getWeight() >= 10) {
                            strength = 4;
                        } else {
                            if (input.getWeight() >= 20) {
                                strength = 5;
                            }
                        }
                    }
                }
                return strength;
            }
        };
        return tagFunction.apply(tag);
    }


    private List<Tasting> lastTasted;

    public List<Tasting> getLastTasted() {
        if (lastTasted == null) {
            lastTasted = tastingService.getLastTastings(DateInterval.getLastMonths(6));
        }
        return lastTasted;
    }

    public void setLastTasted(List<Tasting> lastTasted) {
        this.lastTasted = lastTasted;
    }

    private List<Tasting> bestTastings;

    public List<Tasting> getBestTastings() {
        if (bestTastings == null) {
            setBestTastings(tastingService.getBestTastings(10));
        }
        return bestTastings;
    }

    public void setBestTastings(List<Tasting> bestTastings) {
        this.bestTastings = bestTastings;
    }


}

