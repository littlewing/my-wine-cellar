/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author touret-a
 */
@Named("applicationManagedBean")
@ApplicationScoped
public class ApplicationManagedBean implements Serializable {

    private String baseUrl;

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public List<String> getWorkflowHelpList() {
        List<String> list = new ArrayList<String>();
        ResourceBundle bundle = ResourceBundle.getBundle("messages", ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getLocale());
        list.add(bundle.getString("standardhelpproducers"));
        list.add(bundle.getString("standardhelpbottles"));
        list.add(bundle.getString("standardhelptastings"));
        return list;
    }

    public String getBaseUrl() {
        if (baseUrl == null) {
            StringBuffer buffer = new StringBuffer();
            buffer.append(FacesContext.getCurrentInstance().getExternalContext().getRequestScheme()).append("://")
                    .append(FacesContext.getCurrentInstance().getExternalContext().getRequestServerName())
                    .append(":").append(FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort())
                    .append(((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext()).getContextPath());
            baseUrl = buffer.toString();
        }
        return baseUrl;
    }
}
