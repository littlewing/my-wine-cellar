/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui;

import info.touret.mwc.gui.util.ViewScoped;
import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.Producer;
import info.touret.mwc.model.Tag;
import info.touret.mwc.service.BottleService;
import info.touret.mwc.service.BusinessService;
import info.touret.mwc.service.ProducerService;
import info.touret.mwc.service.TagService;

import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author touret-a
 */
@Named("bottleManagedBean")
@ViewScoped
public class BottleManagedBean extends CRUDManagedBean<Bottle> {

    @EJB
    BottleService bottleservice;
    @EJB
    ProducerService producerService;
    @EJB
    TagService tagService;

    private List<Producer> producers;

    public List<Producer> getProducers() {
        if (producers == null) {
            setProducers(producerService.list(Producer.class));
        }
        return producers;
    }

    public void setProducers(List<Producer> producers) {
        this.producers = producers;
    }

    private Producer selectProducer;

    public Producer getSelectProducer() {
        return selectProducer;
    }

    private Integer selectProducerId;

    public Integer getSelectProducerId() {
        return selectProducerId;
    }

    public void setSelectProducerId(Integer selectProducerId) {
        this.selectProducerId = selectProducerId;
    }

    public void setSelectProducer(Producer _selectProducer) {
        this.selectProducer = _selectProducer;
    }

    private List<SelectItem> producerItems;

    public List<SelectItem> getProducerItems() {
        if (producerItems == null) {
            producerItems = new ArrayList<SelectItem>();
            for (Producer current : getProducers()) {
                producerItems.add(new SelectItem(current.getId(), current.getName()));
            }
        }
        return producerItems;
    }

    public void setProducerItems(List<SelectItem> producerItems) {
        this.producerItems = producerItems;
    }

    public List<Tag> getAllTags() {
        List<Tag> tags = new ArrayList<Tag>();
        tags = tagService.list(Tag.class);
        return tags;
    }


    @Override
    protected Bottle newInstance() {
        return new Bottle();
    }

    @Override
    protected String url() {
        return "/faces/bottlemgmt";
    }

    @Override
    protected String getEntityDescription() {
        return "La bouteille " + getEntity().getName();
    }

    @Override
    protected BusinessService<Bottle> getService() {
        return bottleservice;
    }

    @Override
    protected Class<Bottle> getDomainClass() {
        return Bottle.class;
    }

    public List<Producer> searchProducers(String query) {
        Producer p = new Producer();
        p.setName(query);
        return producerService.findLikeBySelectedAttributes(Producer.class, p, "name");
    }

    @Override
    public void saveNew(javax.faces.event.ActionEvent event) {
        getEntity().setProducer(producerService.findByID(Producer.class, getSelectProducerId()));
        super.saveNew(event);
    }

    @Deprecated
    public String getGoogleCalendarLink() {
        String originalLink = "http://www.google.com/calendar/event?action=TEMPLATE&text=@@TEXTE@@&dates=@@DATE@@/@@DATE@@&details=&location=&trp=false&sprop=my-wine-cellar.touret.info&sprop=name:my%20wine%20cellar";
        String link = originalLink.replace("@@TEXTE@@", getEntity().getName());
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String date = format.format(getEntity().getTodrink());
        link = link.replace("@@DATE@@", date);
        return link;
    }

    @Override
    protected String[] getFilterColumns() {
        return new String[]{"name", "denomination"};
    }

    @Override
    protected Bottle getDefaultDomainObject() {
        return new Bottle();
    }

    @Override
    public Bottle getEntity() {
        Bottle bottle = super.getEntity();
        if (bottle.getVintage() == 0) {
            bottle.setVintage(GregorianCalendar.getInstance().get(Calendar.YEAR));
            bottle.setAcquired(new Date());
        }
        return bottle;
    }
}
