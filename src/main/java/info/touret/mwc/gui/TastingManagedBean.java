/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.gui;


import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.Tasting;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BottleService;
import info.touret.mwc.service.BusinessService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Named;

/**
 * @author touret-a
 */
@Named("tastingManagedBean")
@RequestScoped
public class TastingManagedBean extends CRUDManagedBean<Tasting> {

    @EJB
    private BusinessService<Tasting> tastingService;

    @EJB
    private BottleService bottleService;

    private UserWine user;

    public UserWine getUser() {
        return user;
    }

    public void setUser(UserWine user) {
        this.user = user;
    }

    private Bottle selectBottle;

    public Bottle getSelectBottle() {
        if (selectBottle == null) {
            setSelectBottle(new Bottle());
        }
        return selectBottle;
    }

    public void setSelectBottle(Bottle _selectBottle) {
        this.selectBottle = _selectBottle;
    }

    @Override
    protected Tasting newInstance() {
        return new Tasting();
    }

    @Override
    protected String url() {
        return "/faces/tastingmgmt";
    }

    @Override
    protected String getEntityDescription() {
        return "la dégustation" + getEntity().getName();
    }

    @Override
    protected BusinessService<Tasting> getService() {
        return tastingService;
    }

    @Override
    protected Class<Tasting> getDomainClass() {
        return Tasting.class;
    }

    public void validateBottle() {
    }

    @Override
    public void saveNew(ActionEvent event) {
        getEntity().setBottle(bottleService.findByID(Bottle.class, getSelectBottleId()));
        getEntity().setName(getEntity().getBottle().getId() + "-" + getEntity().getTastingdate());
        super.saveNew(event);
    }


    private Integer selectBottleId;

    public Integer getSelectBottleId() {
        return selectBottleId;
    }

    public void setSelectBottleId(Integer selectBottleId) {
        this.selectBottleId = selectBottleId;
    }

    @Override
    protected String[] getFilterColumns() {
        return new String[]{"name", "tastingdate"};
    }

    @Override
    protected Tasting getDefaultDomainObject() {
        return new Tasting();
    }
}
