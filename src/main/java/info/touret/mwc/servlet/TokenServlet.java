package info.touret.mwc.servlet;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.extensions.servlet.auth.oauth2.AbstractAuthorizationCodeServlet;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.util.OAuthAuthorizationHelper;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: touret-a
 * Date: 23/10/12
 * Time: 10:26
 */
@WebServlet(name = "OAUTH Token Servlet", urlPatterns = {"/oauth2"}, asyncSupported = true)
public class TokenServlet extends AbstractAuthorizationCodeServlet {

    @Inject
    OAuthAuthorizationHelper oAuthAuthorizationHelper;

    @Inject
    UserWine userWine;

    @Override
    protected AuthorizationCodeFlow initializeFlow() throws ServletException, IOException {
        return oAuthAuthorizationHelper.newFlow();
    }

    @Override
    protected String getRedirectUri(HttpServletRequest httpServletRequest) throws ServletException, IOException {
        return oAuthAuthorizationHelper.getRedirectUri(httpServletRequest);
    }

    @Override
    protected String getUserId(HttpServletRequest httpServletRequest) throws ServletException, IOException {
        return userWine.getUsername();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/protected/home.xhtml");
    }
}
