/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.servlet;


//import com.google.gdata.client.authn.oauth.OAuthException;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.servlet.auth.oauth2.AbstractAuthorizationCodeCallbackServlet;
import info.touret.mwc.gui.UserManagedBean;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;
import info.touret.mwc.util.Logged;
import info.touret.mwc.util.OAuthAuthorizationHelper;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;


/**
 * @author touret-a
 */
@WebServlet(name = "OAUTH Token Callback Servlet", urlPatterns = {"/oauth2callback"}, asyncSupported = true)
public class TokenCallbackServlet extends AbstractAuthorizationCodeCallbackServlet {

    @Inject
    private Logger trace;
//    @Inject
//    private GoogleOAuth googleOAuth;

    @Inject
    UserManagedBean userManagedBean;

    @Inject
    BusinessService<UserWine> userWineBusinessService;

    @Logged
    @Inject
    UserWine userWine;

//    @Override
//    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        try {
//            trace.fine("----> debut token servlet");
////            String token = googleOAuth.getSessionToken(req.getQueryString());
////            trace.fine("----> token = " + token);
////            userWine.setToken(token);
//            trace.fine("----> on va sauver le tout !!!");
//            trace.fine("userWine =" + userWine);
//            userWine = userWineBusinessService.update(userWine);
//            userManagedBean.setUser(userWine);
//            resp.sendRedirect(getServletContext().getContextPath() + "/faces/protected/home.xhtml");
////        } catch (OAuthException e) {
////            trace.severe(e.getMessage());
////            FacesContext.getCurrentInstance().addMessage("null", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Google ERROR", "Google ERROR"));
////        }
//        }
//          catch (Exception e) {
//            trace.severe(e.getMessage());
//        }
//    }

    @Inject
    OAuthAuthorizationHelper oAuthAuthorizationHelper;


    @Override
    protected AuthorizationCodeFlow initializeFlow() throws ServletException, IOException {
        return oAuthAuthorizationHelper.newFlow();
    }

    @Override
    protected String getRedirectUri(HttpServletRequest httpServletRequest) throws ServletException, IOException {
        return oAuthAuthorizationHelper.getRedirectUri(httpServletRequest);
    }

    @Override
    protected String getUserId(HttpServletRequest httpServletRequest) throws ServletException, IOException {
        return userWine.getUsername();
    }

    @Override
    protected void onSuccess(HttpServletRequest req, HttpServletResponse resp, Credential credential) throws ServletException, IOException {
        resp.sendRedirect("/protected/home.xhtml");
    }
}
