package info.touret.mwc.servlet;

import com.google.apps.easyconnect.easyrp.client.basic.Context;
import com.google.apps.easyconnect.easyrp.client.basic.data.AccountService;
import com.google.apps.easyconnect.easyrp.client.basic.session.RpConfig;
import com.google.apps.easyconnect.easyrp.client.basic.session.SessionBasedSessionManager;
import com.google.apps.easyconnect.easyrp.client.basic.session.SessionManager;
import info.touret.mwc.util.UserWineAccountService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * User: touret-a
 * Date: 03/07/12
 * Time: 16:10
 */
@WebListener
public class GitKitContextListener implements ServletContextListener {


    public static final String SESSION_KEY_LOGIN_USER = "login_account";
    /**
     * Default URL for home page.
     */
    public static final String HOME_PAGE_URL = "faces/protected/home.xhtml";

    /**
     * The URL for account page.
     */
    public static final String ACCOUNT_PAGE_URL = "faces/protected/home.xhtml";

    /**
     * The URL for signup page.
     */
    public static final String SIGNUP_PAGE_URL = "faces/protected/home.xhtml";

    /**
     * The URL for DBMS management page.
     */
    public static final String DBMS_PAGE_URL = "faces/protected/home.xhtml";

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        initEasyRpContext();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

    /**
     * */
    private void initEasyRpContext() {

        try {
            RpConfig config = new RpConfig.Builder().sessionUserKey(SESSION_KEY_LOGIN_USER)
                    .homeUrl(HOME_PAGE_URL).signupUrl(SIGNUP_PAGE_URL).loginUrl(ACCOUNT_PAGE_URL).build();
            AccountService accountService = new UserWineAccountService();
            SessionManager sessionManager = new SessionBasedSessionManager(config);
            Context.setConfig(config);
            Context.setAccountService(accountService);
            Context.setSessionManager(sessionManager);
            Context.setGoogleApisDeveloperKey("AIzaSyAfgjWnM4KYkdkQ3sCd0PAdgmAiU6ZhFa8");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
