/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service;

import info.touret.mwc.model.Tag;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author touret-a
 */
@LocalBean
@Stateless
public class TagService extends BusinessService<Tag> {

    public Map<String, Tag> getIndexedTags(List<Tag> tags) {
        assert tags != null;
        Map<String, Tag> indexedTags = new HashMap<String, Tag>(tags.size());
        for (Tag current : tags) {
            indexedTags.put(current.getName(), current);
        }
        return indexedTags;
    }

    public Map<String, Tag> getIndexedTags() {
        return getIndexedTags(list(Tag.class));
    }

    public Tag findByName(String name) {
        Tag tag = null;
        Query query = em.createNamedQuery("findByName").setParameter("name", name);
        List<Tag> tags = query.getResultList();
        if (tags != null && !tags.isEmpty()) {
            tag = tags.get(0);
        }
        return tag;
    }


}
