/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package info.touret.mwc.service;

/**
 * @author touret-a
 */
public class ReminderException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -910159817290895711L;

    public ReminderException(Throwable cause) {
        super(cause);
    }

    public ReminderException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReminderException(String message) {
        super(message);
    }

    public ReminderException() {
    }

}
