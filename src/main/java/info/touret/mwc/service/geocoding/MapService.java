package info.touret.mwc.service.geocoding;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import info.touret.mwc.model.Producer;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 10:55
 */
@LocalBean
@Stateless
public class MapService {

    private final static String GEOCODING_URL_PREFIX = "http://maps.googleapis.com/maps/api/geocode/json?address=";
    private final static String GEOCODING_URL_SUFFIX = "&sensor=true";

    @Inject
    private Logger trace;

    public Producer geolocalize(Producer producer) {
        if (isProducerLocalizable(producer)) {
            URL url = getGeocodingURL(producer);
            try {
                Gson gson = new GsonBuilder().create();
                URLConnection connection = url.openConnection();
                GeocoderResponse response = gson.fromJson(new InputStreamReader(connection.getInputStream(), "utf-8"), GeocoderResponse.class);
                if (response.getResults().size() > 0) {
                    producer.setLongitude(response.getResults().get(0).getGeometry().getLocation().getLng());
                    producer.setLattitude(response.getResults().get(0).getGeometry().getLocation().getLat());
                }
            } catch (IOException e) {
                trace.severe(e.getMessage());
                throw new GeocodingException(e);
            }


        }
        return producer;
    }


    private Boolean isProducerLocalizable(Producer producer) {
        Boolean isLocalizable = true;
        if (isNullOrEmpty(producer.getCity()) && isNullOrEmpty(producer.getCountry())) {
            isLocalizable = false;
        }
        return isLocalizable;
    }

    private URL getGeocodingURL(Producer producer) {
        URL url = null;
        StringBuffer buffer = new StringBuffer(GEOCODING_URL_PREFIX);

        if (!isNullOrEmpty(producer.getAddress())) {
            buffer.append(producer.getAddress().replace(" ", "+")).append(",");
        }
        if (!isNullOrEmpty(producer.getCity())) {
            buffer.append(producer.getCity().replace(" ", "+")).append(",");
        }
        if (!isNullOrEmpty(producer.getZip())) {
            buffer.append(producer.getZip().replace(" ", "+")).append(",");
        }
        if (!isNullOrEmpty(producer.getCountry())) {
            buffer.append(producer.getCountry());
        }
        buffer.append(GEOCODING_URL_SUFFIX);
        try {
            url = new URL(buffer.toString());
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
        return url;
    }
}
