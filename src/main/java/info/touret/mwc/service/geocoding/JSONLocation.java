package info.touret.mwc.service.geocoding;

/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 13:52
 */
public class JSONLocation {
    private float lat;
    private float lng;

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }
}
