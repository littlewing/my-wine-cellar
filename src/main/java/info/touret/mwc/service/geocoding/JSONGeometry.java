package info.touret.mwc.service.geocoding;

/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 13:52
 */
public class JSONGeometry {
    private JSONLocation location;

    public JSONLocation getLocation() {
        return location;
    }

    public void setLocation(JSONLocation location) {
        this.location = location;
    }
}
