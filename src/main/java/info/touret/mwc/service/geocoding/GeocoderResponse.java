package info.touret.mwc.service.geocoding;

import java.util.List;

/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 13:42
 */
public class GeocoderResponse {
    private List<JSONResult> results;

    public List<JSONResult> getResults() {
        return results;
    }

    public void setResults(List<JSONResult> results) {
        this.results = results;
    }
}

