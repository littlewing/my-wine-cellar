package info.touret.mwc.service.geocoding;

/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 13:48
 */
public class JSONResult {
    private JSONGeometry geometry;

    public JSONGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(JSONGeometry geometry) {
        this.geometry = geometry;
    }
}
