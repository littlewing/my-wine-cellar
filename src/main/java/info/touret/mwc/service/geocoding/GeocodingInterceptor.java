package info.touret.mwc.service.geocoding;

import info.touret.mwc.model.Producer;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

/**
 * Intercepte une insertion / mise à jour d un producteur et geocalise
 * User: touret-a
 * Date: 18/07/12
 * Time: 14:09
 */
public class GeocodingInterceptor {

    @Inject
    private MapService mapService;

    @Inject
    Logger trace;

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        Object invoker = context.proceed();
        Producer producer = (Producer) context.getParameters()[0];
        if (producer.getLattitude() == null || producer.getLongitude() == null) {
            trace.info("tentative de geolocalisation...");
            producer = mapService.geolocalize(producer);
            context.setParameters(new Object[]{producer});
        }
        return invoker;
    }
}
