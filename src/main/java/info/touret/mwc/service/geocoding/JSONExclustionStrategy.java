package info.touret.mwc.service.geocoding;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 13:45
 */
public class JSONExclustionStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return !fieldAttributes.getName().equalsIgnoreCase("location");
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
