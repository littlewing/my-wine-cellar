package info.touret.mwc.service.geocoding;

/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 11:46
 */
public class GeocodingException extends RuntimeException {
    public GeocodingException() {
    }

    public GeocodingException(String message) {
        super(message);
    }

    public GeocodingException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeocodingException(Throwable cause) {
        super(cause);
    }
}
