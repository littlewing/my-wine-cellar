/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service;

import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.DateInterval;
import info.touret.mwc.model.Tag;
import info.touret.mwc.util.Remindable;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author touret-a
 */
@LocalBean
@Stateless
public class BottleService extends BusinessService<Bottle> {

    public List<Bottle> getBottleList() {
        List<Bottle> bottles = new ArrayList<Bottle>();
        bottles = em.createQuery("select b from Bottle b").getResultList();
        return bottles;
    }

    public List<Bottle> findBottlesByTag(Tag tag) {
        List<Bottle> bottles = new ArrayList<Bottle>();
        Query query = em.createQuery("select b from Bottle b join b.tags t where t.id= :tagname");
        query.setParameter("tagname", tag.getId());
        return bottles;
    }

    @Inject
    TagService tagService;
    @Inject
    ProducerService producerService;

    /**
     * Vérifie l existence en base des tags incremente le poids du nombre de bouteille et sauve
     */

    private static Logger trace = Logger.getLogger(BottleService.class.getCanonicalName());

    @Override
    @Remindable
    public void save(Bottle pojo) {
        if (pojo != null) {
            if (pojo.getTags() != null || !pojo.getTags().isEmpty()) {
                Map<String, Tag> indexedTags = tagService.getIndexedTags(pojo.getTags());
                for (Tag tag : pojo.getTags()) {
                    Tag persistedTag = tagService.findByName(tag.getName());
                    if (persistedTag != null) {
                        indexedTags.remove(pojo.getName());
                        persistedTag.setWeight(persistedTag.getWeight() + pojo.getQuantity());
                        indexedTags.put(persistedTag.getName(), persistedTag);
                    }
                }
                pojo.setTags(new ArrayList<Tag>(indexedTags.values()));
            }

            // différence hibernate eclipselink
            if (pojo.getProducer().getId() >= 0) {
                pojo.setProducer(em.merge(pojo.getProducer()));
            }
            super.save(pojo);
        }
    }

    /**
     * Retourne la liste des bouteilles bues par date
     *
     * @param interval intervalle
     * @return la liste
     */
    public List<Bottle> findBottlesDrunkByDate(DateInterval interval) {
        List<Bottle> bottles = new ArrayList<Bottle>();
        Query query = em.createNamedQuery("findByDateInterval");
        query.setParameter("dateMin", interval.getMin());
        query.setParameter("dateMax", interval.getMax());
        bottles = query.getResultList();
        return bottles;
    }


}
