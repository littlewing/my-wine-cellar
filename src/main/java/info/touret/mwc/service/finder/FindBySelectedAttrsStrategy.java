package info.touret.mwc.service.finder;

import info.touret.mwc.model.Identifier;

import javax.enterprise.inject.Produces;

/**
 * User: touret-a
 * Date: 17/07/12
 * Time: 16:34
 */

public class FindBySelectedAttrsStrategy<T extends Identifier> {
    @Equals
    @Produces
    public FinderBySelectedAttrs<T> getEqualsFinder() {
        return new EqualsFinderBySelectedAttrs<T>();
    }

    @Like
    @Produces
    public FinderBySelectedAttrs<T> getLikeFinder() {
        return new LikeFinderBySelectedAttrs<T>();
    }

}
