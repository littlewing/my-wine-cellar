package info.touret.mwc.service.finder;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Qualifier pour les recherches de type like
 * User: touret-a
 * Date: 17/07/12
 * Time: 16:41
 */
@Qualifier
@Retention(RUNTIME)
@Target({TYPE, FIELD, METHOD})
public @interface Like {
}
