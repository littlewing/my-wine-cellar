package info.touret.mwc.service.finder;

import info.touret.mwc.model.Identifier;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;

/**
 * Effectue des recherches en effectuant un equals
 * User: touret-a
 * Date: 17/07/12
 * Time: 16:52
 */
public class EqualsFinderBySelectedAttrs<T extends Identifier> extends AbstractFinderBySelectedAttrs<T> {
    @Override
    protected Predicate createPredicate(CriteriaBuilder criteriaBuilder, Root root, Field field, T t) {
        try {
            return criteriaBuilder.equal(root.get(field.getName()), field.get(t).toString());
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected Predicate createPredicateFullText(CriteriaBuilder criteriaBuilder, Root root, Field field, T t, String query) {
        try {
            return criteriaBuilder.or(criteriaBuilder.equal(root.get(field.getName()), field.get(t).toString()));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
