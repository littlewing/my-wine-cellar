package info.touret.mwc.service.finder;

import info.touret.mwc.model.Identifier;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Interface de recherche
 * User: touret-a
 * Date: 17/07/12
 * Time: 17:00
 */
public interface FinderBySelectedAttrs<T extends Identifier> {
    List<T> find(EntityManager em, Class<T> aClass, T t, String... attributes);

    List<T> findFullText(EntityManager em, Class<T> aClass, T t, String query, String... attributes);
}
