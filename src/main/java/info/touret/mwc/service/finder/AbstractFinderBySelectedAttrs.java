package info.touret.mwc.service.finder;

import info.touret.mwc.model.Identifier;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe centralisant la logique de creation d une requete dynamique
 * User: touret-a
 * Date: 17/07/12
 * Time: 16:52
 */
public abstract class AbstractFinderBySelectedAttrs<T extends Identifier> implements FinderBySelectedAttrs<T> {
    @Override
    public List<T> find(EntityManager em, Class<T> aClass, T t, String... attributes) {
        List<T> results = new ArrayList<T>();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(aClass);
        Root from = criteriaQuery.from(aClass);
        List<Predicate> expressions = new ArrayList<Predicate>();
        for (String current : attributes) {
            try {
                Field currentField = t.getClass().getDeclaredField(current);
                currentField.setAccessible(true);
                expressions.add(createPredicate(criteriaBuilder, from, currentField, t));
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        }
        criteriaQuery.where(expressions.toArray(new Predicate[attributes.length]));
        results.addAll(em.createQuery(criteriaQuery).getResultList());
        return results;
    }

    /**
     * Logique abstraite de creation du predicat : like ou equals
     *
     * @param criteriaBuilder
     * @param field
     * @param root
     * @param t
     * @return le predicat genere
     * @throws RuntimeException
     */
    protected abstract Predicate createPredicateFullText(CriteriaBuilder criteriaBuilder, Root root, Field field, T t, String query);


    protected abstract Predicate createPredicate(CriteriaBuilder criteriaBuilder, Root root, Field field, T t);

    @Override
    public List<T> findFullText(EntityManager em, Class<T> aClass, T t, String query, String... attributes) {
        List<T> results = new ArrayList<T>();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(aClass);
        Root from = criteriaQuery.from(aClass);
        List<Predicate> expressions = new ArrayList<Predicate>();
        for (String current : attributes) {
            try {
                Field currentField = t.getClass().getDeclaredField(current);
                currentField.setAccessible(true);
                expressions.add(createPredicateFullText(criteriaBuilder, from, currentField, t, query));
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        }
        criteriaQuery.where(criteriaBuilder.or(expressions.toArray(new Predicate[attributes.length])));
        results.addAll(em.createQuery(criteriaQuery).getResultList());
        return results;
    }
}
