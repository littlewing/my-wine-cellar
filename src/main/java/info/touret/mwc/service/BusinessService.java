/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service;

import info.touret.mwc.model.Identifier;
import info.touret.mwc.service.finder.Equals;
import info.touret.mwc.service.finder.FinderBySelectedAttrs;
import info.touret.mwc.service.finder.Like;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author touret-a
 */
@LocalBean
@Stateless
public class BusinessService<T extends Identifier> {
    private final static Logger trace = Logger.getLogger("BusinessService");

    @PersistenceContext
    EntityManager em;

    public List<T> list(Class<T> pojoclass) {
        List<T> pojos = new ArrayList<T>();
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(pojoclass);
            Root from = criteriaQuery.from(pojoclass);
            CriteriaQuery<T> select = criteriaQuery.select(from);
            TypedQuery<T> typedQuery = em.createQuery(select);
            pojos.addAll(typedQuery.getResultList());
        } catch (Exception e) {
            trace.log(Level.SEVERE, "La récupération de la liste de " + pojoclass + " a echouee", e);
            throw new ServiceException(e);
        }
        return pojos;
    }

    public void save(T pojo) {
        try {
            em.persist(pojo);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }


    public void delete(T object) {
        try {
            object = (T) em.find(object.getClass(), object.getId());
            if (object != null) {
                em.remove(object);
            }
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }


    public T update(T pojo) {
        try {
            return em.merge(pojo);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    public <T> T findByID(Class<T> aClass, Integer key) {
        try {
            return em.find(aClass, key);
        } catch (Exception e) {
            throw new ServiceException(e);
        }

    }

    @Inject
    @Equals
    private FinderBySelectedAttrs<T> equalsFinder;

    @Inject
    @Like
    private FinderBySelectedAttrs<T> likeFinder;

    public List<T> findLikeBySelectedAttributes(Class<T> aClass, T object, String... attributes) {
        return likeFinder.find(em, aClass, object, attributes);
    }

    public List<T> findEqualBySelectedAttributes(Class<T> aClass, T object, String... attributes) {
        return equalsFinder.find(em, aClass, object, attributes);
    }

    public List<T> findLikeFullText(Class<T> aClass, T object, String query, String... attributes) {
        return likeFinder.findFullText(em, aClass, object, query, attributes);
    }


}
