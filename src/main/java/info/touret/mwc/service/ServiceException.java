package info.touret.mwc.service;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 09/12/11
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
public class ServiceException extends RuntimeException {
    public ServiceException() {
        super();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public ServiceException(String message) {
        super(message);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public ServiceException(Throwable cause) {
        super(cause);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
