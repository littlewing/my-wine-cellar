/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.util.Logged;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;


/**
 * @author touret-a
 */
@LocalBean
@Stateless
public class ReminderService {

    @Inject
    private Calendar calendarService;


    @Logged
    @Inject
    UserWine currentUser;

    public void createEvent(Bottle bottle)
            throws ReminderException {
        if (currentUser == null) {
            throw new ReminderException(
                    "L'utilisateur est NULL n est pas defini");
        }
        String sessionToken = currentUser.getToken();
        try {
            if (sessionToken != null) {
                trace.info("definition des url");

                Event myEntry = new Event();
                myEntry.setId(bottle.getName());
                myEntry.setSummary(bottle.getName()
                        + " - " + bottle.getVintage() + " - "
                        + bottle.getDenomination());

                trace.info("definition des dates");
                /*Definition des dates */
                Date startDate = bottle.getTodrink();
                Date endDate = new Date(startDate.getTime() + 3600000);
                DateTime startDateTime = new DateTime(startDate, TimeZone.getTimeZone("UTC"));
                DateTime endDateTime = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
                EventDateTime startTime = new EventDateTime().setDateTime(startDateTime);
                EventDateTime endTime = new EventDateTime().setDate(endDateTime);
                myEntry.setStart(startTime).setEnd(endTime);
                trace.info("Ajout du reminder");
                // ajout d une alerte
                addReminders(myEntry);
                trace.info("Insertion de l entree dans le calendrier google");
                Event insertedEntry = calendarService.events().insert("{primary}", myEntry).execute();
                trace.info("Evenement cree : ID: [" + insertedEntry.getId() + "] - LINK" + insertedEntry.getHtmlLink());
            } else {
                trace.warning("Le session token de l'utilisateur [" + currentUser + "] est null!");
            }
        } catch (IOException ex) {
            trace.severe(ex.getMessage());
            throw new ReminderException(ex.getLocalizedMessage());
        } catch (ServiceException ex) {
            trace.severe(ex.getMessage());
            throw new ReminderException(ex.getLocalizedMessage());
        } catch (Exception ex) {
            trace.severe(ex.getMessage());
            throw new ReminderException(ex.getLocalizedMessage());
        }
    }

    @Inject
    private Logger trace;

    private void addReminders(Event _entry) {
        try {
            _entry.setReminders(new Event.Reminders().setUseDefault(true));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
