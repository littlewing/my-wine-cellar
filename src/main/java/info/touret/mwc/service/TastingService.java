package info.touret.mwc.service;

import info.touret.mwc.model.DateInterval;
import info.touret.mwc.model.Tasting;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 22/11/11
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
@LocalBean
@Stateless
public class TastingService extends BusinessService<Tasting> {
    @Override
    public void save(Tasting pojo) {
        pojo.getBottle().setQuantity(pojo.getBottle().getQuantity() - pojo.getQuantity());
        super.save(pojo);

    }

    public List<Tasting> getLastTastings(DateInterval interval) {
        Query query = em.createNamedQuery("findLastTastings");
        query.setParameter("fromdate", interval.getMin());
        query.setParameter("todate", interval.getMax());
        return query.getResultList();
    }


    public List<Tasting> getBestTastings(Integer maxElements) {
        Query query = em.createNamedQuery("findBestTastings");
        query.setMaxResults(maxElements);
        return query.getResultList();
    }
}
