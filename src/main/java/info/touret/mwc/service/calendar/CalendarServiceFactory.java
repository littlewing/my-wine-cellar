/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service.calendar;


import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.util.JPACredentialStore;
import info.touret.mwc.util.Logged;
import info.touret.mwc.util.OAuthAuthorizationHelper;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.IOException;
import java.util.logging.Logger;

public class CalendarServiceFactory {
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();

    private final static Logger trace = Logger.getLogger(CalendarServiceFactory.class.getName());

    @Logged
    @Inject
    private UserWine user;

    @Inject
    private JPACredentialStore jpaCredentialStore;

    @Inject
    private OAuthAuthorizationHelper oAuthAuthorizationHelper;

    @Produces
    public Calendar get() {
        Calendar calendar = null;
        try {
            trace.info("----->" + user);
            calendar = oAuthAuthorizationHelper.loadCalendarClient();
        } catch (IOException e) {
            trace.warning(e.getMessage());
        }
        return calendar;
    }
}
