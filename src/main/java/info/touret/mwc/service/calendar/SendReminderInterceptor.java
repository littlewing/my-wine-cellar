/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service.calendar;

import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.ReminderService;
import info.touret.mwc.util.Logged;
import info.touret.mwc.util.Remindable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

/**
 * @author touret-a
 */
@Remindable
@Interceptor
public class SendReminderInterceptor {

    @Inject
    private ReminderService reminderService;

    @Logged
    @Inject
    UserWine currentUser;

    @Inject
    private Logger trace;

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        Object invoker = context.proceed();
        // todo voir si je conserve !
        if (context.getParameters().length == 1 && (context.getParameters()[0] instanceof Bottle)) {
            Bottle bottle = (Bottle) context.getParameters()[0];
            trace.info("Creation d'un evenement " + bottle.toString() + " - "
                    + currentUser.toString());
            reminderService.createEvent(bottle);
            trace.info("Evenement cree pour " + bottle.toString());
        }
        return invoker;
    }
}
