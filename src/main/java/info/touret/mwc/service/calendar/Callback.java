package info.touret.mwc.service.calendar;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * User: touret-a
 * Date: 11/04/12
 * Time: 15:07
 */
@Named
@SessionScoped
public class Callback implements Serializable {
    String url;

    public String getUrl() {
        return url;
    }

    public Callback setUrl(String url) {
        this.url = url;
        return this;
    }
}
