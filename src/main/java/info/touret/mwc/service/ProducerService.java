package info.touret.mwc.service;

import info.touret.mwc.model.Producer;
import info.touret.mwc.service.geocoding.GeocodingInterceptor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

/**
 * User: touret-a
 * Date: 18/07/12
 * Time: 14:14
 */
@LocalBean
@Stateless
public class ProducerService extends BusinessService<Producer> {
    @Interceptors({GeocodingInterceptor.class})
    @Override
    public void save(Producer pojo) {
        super.save(pojo);
    }

    @Interceptors({GeocodingInterceptor.class})
    @Override
    public Producer update(Producer pojo) {
        return super.update(pojo);
    }
}

