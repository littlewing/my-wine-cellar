package info.touret.mwc.util;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * User: touret-a
 * Date: 02/02/12
 * Time: 15:48
 */
@WebFilter(filterName = "loginFilter", urlPatterns = {"/faces/protected/*", "/index.html"})
public class LoginFilter implements Filter {
    private FilterConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        config = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        Boolean redirectToLoginPage = false;

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        if (!httpServletRequest.isRequestedSessionIdValid()) {
            redirectToLoginPage = true;
        }
        if (redirectToLoginPage) {
            logger.warning(" /!\\ Session Invalide --> retour vers l identification");
            ((HttpServletResponse) servletResponse).sendRedirect(config.getServletContext().getContextPath() + "/faces/login.xhtml");
        } else {
            logger.fine("Session valide ");
            logger.fine("Utilisateur concerne : " + httpServletRequest.getSession(false).getAttribute("login_account"));
            filterChain.doFilter(servletRequest, servletResponse);
        }

    }

    @Override
    public void destroy() {
        config = null;
    }

    private static final Logger logger = Logger.getLogger(LoginFilter.class.getName());

}
