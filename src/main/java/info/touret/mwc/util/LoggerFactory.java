package info.touret.mwc.util;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import java.util.logging.Logger;


/**
 * User: touret-a
 * Date: 02/04/12
 * Time: 17:13
 */
@Named
public class LoggerFactory {

    @Produces
    public Logger getLogger(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getClass().getCanonicalName());
    }
}
