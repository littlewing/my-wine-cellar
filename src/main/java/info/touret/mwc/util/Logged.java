package info.touret.mwc.util;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * User: touret-a
 * Date: 02/04/12
 * Time: 17:12
 */

@Qualifier
@Retention(RUNTIME)
@Target({PARAMETER, METHOD, FIELD})
public @interface Logged {
}
