package info.touret.mwc.util;

import com.google.apps.easyconnect.easyrp.client.basic.data.Account;

/**
 * User: touret-a
 * Date: 18/05/12
 * Time: 14:47
 */
public class UserWineAccount implements Account {

    private String email;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setFederated(boolean federated) {
        this.federated = federated;
    }

    private boolean federated;
    private String displayName;
    private String photoUrl;


    @Override
    public boolean isFederated() {
        return federated;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getPhotoUrl() {
        return photoUrl;
    }

    public UserWineAccount(String email) {
        this.email = email;
        this.displayName = email;
    }


    public UserWineAccount() {
    }

    @Override
    public String toString() {
        return "UserWineAccount{" +
                "email='" + email + '\'' +
                ", federated=" + federated +
                ", displayName='" + displayName + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                '}';
    }
}
