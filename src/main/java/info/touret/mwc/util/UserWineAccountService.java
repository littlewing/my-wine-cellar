package info.touret.mwc.util;

import com.google.apps.easyconnect.easyrp.client.basic.data.Account;
import com.google.apps.easyconnect.easyrp.client.basic.data.AccountException;
import com.google.apps.easyconnect.easyrp.client.basic.data.AccountService;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * User: touret-a
 * Date: 18/05/12
 * Time: 11:45
 */
public class UserWineAccountService implements AccountService {
    private boolean supportAutoCreateAccount = true;

    public boolean isSupportAutoCreateAccount() {
        return supportAutoCreateAccount;
    }

    public void setSupportAutoCreateAccount(boolean supportAutoCreateAccount) {
        this.supportAutoCreateAccount = supportAutoCreateAccount;
    }

    @Override
    public Account getAccountByEmail(String email) {
        return new UserWineAccount(email);

    }

    @Override
    public Account createFederatedAccount(JSONObject assertion) throws AccountException {
        if (!isSupportAutoCreateAccount()) {
            throw new AccountException(AccountException.ACTION_NOT_ALLOWED);
        }
        UserWineAccount user = new UserWineAccount();
        try {
            user.setEmail(assertion.getString("email"));
//            if (assertion.has("firstName")) {
//                user.setFirstName(assertion.getString("firstName"));
//            } else {
//                user.setFirstName("empty");
//            }
//            if (assertion.has("lastName")) {
//                user.setLastName(assertion.getString("lastName"));
//            } else {
//                user.setLastName("empty");
//            }
            if (assertion.has("profilePicture")) {
                user.setPhotoUrl(assertion.getString("profilePicture"));
            } else {
                user.setPhotoUrl("http://www.google.com/uds/modules/identitytoolkit/image/nophoto.png");
            }
            user.setFederated(true);
//            user.setBirthdayDay("0");
//            user.setBirthdayMonth("0");
//            user.setBirthdayYear("0");

            UserWineWrapper userWineWrapper = new UserWineWrapper(user);
            UserWine userWine = userWineWrapper.toUserWine();

            BusinessService<UserWine> userWineBusinessService = new BusinessService<UserWine>();
            userWineWrapper.setUserWine(userWine);
            userWineBusinessService.save(userWine);

            Account createdUser = userWineWrapper.getAccount();
            return createdUser;
        } catch (JSONException e) {
            throw new AccountException(AccountException.UNKNOWN_ERROR);
        }
    }


    @Override
    public boolean checkPassword(String s, String password) {
        return true;
    }

    @Override
    public void toFederated(String s) throws AccountException {
        UserWineAccount userWineAccount = new UserWineAccount(s);
        userWineAccount.setFederated(true);
    }


}
