package info.touret.mwc.util;

/**
 * User: touret-a
 * Date: 10/07/12
 * Time: 10:57
 */
public class UserWineNotFoundException extends RuntimeException {
    public UserWineNotFoundException() {
    }

    public UserWineNotFoundException(String message) {
        super(message);
    }

    public UserWineNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserWineNotFoundException(Throwable cause) {
        super(cause);
    }
}
