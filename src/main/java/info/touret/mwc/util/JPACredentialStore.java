package info.touret.mwc.util;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialStore;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * User: touret-a
 * Date: 23/10/12
 * Time: 10:37
 */
@Named
public class JPACredentialStore implements CredentialStore {

    private final static Logger trace = Logger.getLogger(JPACredentialStore.class.getName());
    @Inject
    BusinessService<UserWine> userWineBusinessService;

    @Override
    public boolean load(String userId, Credential credential) throws IOException {
        UserWine userWine = new UserWine();
        userWine.setUsername(userId);
        List<UserWine> userWines = userWineBusinessService.findEqualBySelectedAttributes(UserWine.class, userWine, "username");
        return userWines.size() == 1;
    }

    @Override
    public void store(String userId, Credential credential) throws IOException {
        UserWine userWine = new UserWine();
        userWine.setUsername(userId);
        userWine = userWineBusinessService.findEqualBySelectedAttributes(UserWine.class, userWine, "username").get(0);
        userWine.setToken(credential.getRefreshToken());
        userWineBusinessService.update(userWine);
    }

    @Override
    public void delete(String userId, Credential credential) throws IOException {
        UserWine userWine = new UserWine();
        userWine.setUsername(userId);
        userWine = userWineBusinessService.findEqualBySelectedAttributes(UserWine.class, userWine, "username").get(0);
        userWine.setToken("");
        userWineBusinessService.update(userWine);
    }
}
