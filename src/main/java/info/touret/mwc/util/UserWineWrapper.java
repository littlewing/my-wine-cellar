package info.touret.mwc.util;

import com.google.apps.easyconnect.easyrp.client.basic.data.Account;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * User: touret-a
 * Date: 18/05/12
 * Time: 15:24
 */
public class UserWineWrapper implements Serializable {

    private Account account;

    private UserWine userWine;

    @Inject
    BusinessService<UserWine> userWineBusinessService;

    public UserWineWrapper(Account account) {
        this.account = account;
    }

    public UserWineWrapper(UserWine userWine) {
        this.userWine = userWine;
    }

    public UserWineWrapper() {
    }

    public Account toAccount() {
        UserWineAccount userWineAccount = new UserWineAccount(userWine.getUsername());
        return userWineAccount;
    }

    /**
     * Recherche en base l utilisateur correspondant
     */
    public UserWine toUserWine() {
        UserWine userWine = new UserWine();
        userWine.setUsername(account.getEmail());
        List<UserWine> userWines = null;
        try {
            userWines = userWineBusinessService.findEqualBySelectedAttributes(UserWine.class, userWine, "username");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (userWines.isEmpty()) {
            throw new UserWineNotFoundException("User [" + account.getEmail() + "] not found");
        } else {
            userWine = userWines.get(0);
        }
        return userWine;
    }


    public Account getAccount() {
        return account;
    }

    public UserWineWrapper setAccount(Account account) {
        this.account = account;
        return this;
    }

    public UserWine getUserWine() {
        return userWine;
    }

    public void setUserWine(UserWine userWine) {
        this.userWine = userWine;
    }
}
