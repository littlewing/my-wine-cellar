package info.touret.mwc.util;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;
import java.util.logging.Logger;

/**
 * User: touret-a
 * Date: 23/10/12
 * Time: 11:15
 */
@Named
public class OAuthAuthorizationHelper {

    private final static Logger trace = Logger.getLogger(OAuthAuthorizationHelper.class.getName());

    @Inject
    private BusinessService<UserWine> userWineBusinessService;

    @Logged
    @Inject
    private UserWine userWine;


    @Inject
    private JPACredentialStore jpaCredentialStore;

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    /**
     * Global instance of the JSON factory.
     */
    static final JsonFactory JSON_FACTORY = new JacksonFactory();

    private static GoogleClientSecrets clientSecrets = null;

    public GoogleClientSecrets getClientCredential() throws IOException {
        if (clientSecrets == null) {
            clientSecrets = GoogleClientSecrets.load(
                    JSON_FACTORY, OAuthAuthorizationHelper.class.getResourceAsStream("/client_secrets.json"));
        }
        return clientSecrets;
    }

    public String getRedirectUri(HttpServletRequest req) {
        GenericUrl url = new GenericUrl(req.getRequestURL().toString());
        url.setRawPath("/oauth2callback");
        return url.build();
    }

    public GoogleAuthorizationCodeFlow newFlow() throws IOException {
        return new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                getClientCredential(), Collections.singleton(CalendarScopes.CALENDAR)).setCredentialStore(
                jpaCredentialStore).build();
    }

    public Calendar loadCalendarClient() throws IOException {
        Credential credential = newFlow().loadCredential(userWine.getUsername());
        return new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).build();
    }

    /**
     * Returns an {@link IOException} (but not a subclass) in order to work around restrictive GWT
     * serialization policy.
     */
    static IOException wrappedIOException(IOException e) {
        if (e.getClass() == IOException.class) {
            return e;
        }
        return new IOException(e.getMessage());
    }

}
