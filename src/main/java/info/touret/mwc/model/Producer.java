package info.touret.mwc.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 16/11/11
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(schema = "public", name = "producer")
public class Producer implements Identifier {

    private int id;


    @Id
    @SequenceGenerator(name = "PRODUCER_ID_GENERATOR", sequenceName = "producer_id_seq", schema = "public", allocationSize = 1)
    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCER_ID_GENERATOR")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String name;

    @javax.persistence.Column(name = "name", nullable = false, insertable = true, updatable = true, length = 200, precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    private UserWine userWine;

    @JoinColumn(name = "user_wine_id", nullable = true, insertable = true, updatable = true)
    @ManyToOne(optional = true)
    public UserWine getUserWine() {
        return userWine;
    }

    public void setUserWine(UserWine userWine) {
        this.userWine = userWine;
    }

    private String address;

    @javax.persistence.Column(name = "address", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String zip;

    @javax.persistence.Column(name = "zip", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    private String city;

    @javax.persistence.Column(name = "city", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String country;

    @javax.persistence.Column(name = "country", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    private String phone;

    @javax.persistence.Column(name = "phone", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String domain;

    @javax.persistence.Column(name = "domain", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    private String region;

    @javax.persistence.Column(name = "region", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }


    private List<Bottle> bottles;

    @OneToMany(mappedBy = "producer")
    public List<Bottle> getBottles() {
        return bottles;
    }

    public void setBottles(List<Bottle> bottles) {
        this.bottles = bottles;
    }


    public Producer(int id) {
        this.id = id;
    }

    public Producer() {
    }

    private Float lattitude;
    private Float longitude;

    public Float getLattitude() {
        return lattitude;
    }

    public void setLattitude(Float lattitude) {
        this.lattitude = lattitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }
}
