package info.touret.mwc.model;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 08/12/11
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
public interface Identifier extends Serializable {
    public int getId();
}
