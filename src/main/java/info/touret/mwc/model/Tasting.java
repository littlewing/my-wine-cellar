package info.touret.mwc.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(schema = "public", name = "tasting")
@NamedQueries({@NamedQuery(name = "findLastTastings", query = "select t from Tasting  t where t.tastingdate >=:fromdate and t.tastingdate<=:todate"),
        @NamedQuery(name = "findBestTastings", query = "select t from Tasting t order by t.rate desc ")})
public class Tasting implements Identifier {
    private int id;

    @javax.persistence.Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TASTING_ID_GENERATOR")
    @SequenceGenerator(name = "TASTING_ID_GENERATOR", sequenceName = "tasting_id_seq", allocationSize = 1)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String name;

    @javax.persistence.Column(name = "name", nullable = false, insertable = true, updatable = true, length = 200, precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private UserWine userWine;

    @JoinColumn(name = "user_wine_id", nullable = true, insertable = true, updatable = true)
    @ManyToOne
    public UserWine getUserWine() {
        return userWine;
    }

    public void setUserWine(UserWine userWine) {
        this.userWine = userWine;
    }

    private Date tastingdate;

    @javax.persistence.Column(name = "tastingdate", nullable = false, insertable = true, updatable = true, length = 13, precision = 0)
    @Basic
    @Temporal(TemporalType.DATE)
    public Date getTastingdate() {
        return tastingdate;
    }

    public void setTastingdate(Date tastingdate) {
        this.tastingdate = tastingdate;
    }

    private String observation;

    @javax.persistence.Column(name = "observation", nullable = true, insertable = true, updatable = true, length = 256, precision = 0)
    @Basic
    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    private Double rate;

    @javax.persistence.Column(name = "rate", nullable = true, insertable = true, updatable = true, length = 5, precision = 0)
    @Basic
    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    private int quantity;

    @javax.persistence.Column(name = "quantity", nullable = true, insertable = true, updatable = true, length = 10, precision = 0)
    @Basic
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private Bottle bottle;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "bottle_id_seq")
    public Bottle getBottle() {
        return bottle;
    }

    public void setBottle(Bottle bottle) {
        this.bottle = bottle;
    }

}
