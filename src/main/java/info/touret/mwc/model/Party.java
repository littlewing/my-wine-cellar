package info.touret.mwc.model;

import javax.persistence.ManyToOne;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 16/11/11
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
public class Party implements Identifier {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    private UserWine userWineByUserWineId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "user_wine_id", referencedColumnName = "id", table = "party")
    UserWine getUserWineByUserWineId() {
        return userWineByUserWineId;
    }

    public void setUserWineByUserWineId(UserWine userWineByUserWineId) {
        this.userWineByUserWineId = userWineByUserWineId;
    }
}
