package info.touret.mwc.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(schema = "public", name = "bottle")
@NamedQueries({@NamedQuery(name = "findByDateInterval", query = "select distinct b from Bottle b where (b.todrink >= :dateMin and b.todrink<=:dateMax) or (b.acquired >= :dateMin and b.acquired<=:dateMax)"),
        @NamedQuery(name = "findByDrinkDateInterval", query = "select distinct b from Bottle b where (b.todrink >= :dateMin and b.todrink<=:dateMax)")})

public class Bottle implements Identifier {
    private int id;

    @Id
    @javax.persistence.Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BOTTLE_ID_GENERATOR")
    @SequenceGenerator(name = "BOTTLE_ID_GENERATOR", sequenceName = "bottle_id_seq", allocationSize = 1)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    private String name;

    @javax.persistence.Column(name = "name", nullable = false, insertable = true, updatable = true, length = 200, precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private UserWine userWine;

    @JoinColumn(name = "user_wine_id", nullable = true, insertable = true, updatable = true)
    @ManyToOne
    public UserWine getUserWine() {
        return userWine;
    }

    public void setUserWine(UserWine userWine) {
        this.userWine = userWine;
    }

    @Min(value = 0)
    private int quantity;

    @javax.persistence.Column(name = "quantity", nullable = true, insertable = true, updatable = true, length = 10, precision = 0)
    @Basic
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private Date todrink;

    @javax.persistence.Column(name = "todrink", nullable = true, insertable = true, updatable = true, length = 13, precision = 0)
    @Basic
    @Temporal(TemporalType.DATE)
    public Date getTodrink() {
        return todrink;
    }

    public void setTodrink(Date todrink) {
        this.todrink = todrink;
    }

    private Date acquired;

    @javax.persistence.Column(name = "acquired", nullable = true, insertable = true, updatable = true, length = 13, precision = 0)
    @Basic
    @Temporal(TemporalType.DATE)
    public Date getAcquired() {
        return acquired;
    }

    public void setAcquired(Date acquired) {
        this.acquired = acquired;
    }

    private String denomination;

    @javax.persistence.Column(name = "denomination", nullable = true, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    private int vintage;

    @javax.persistence.Column(name = "vintage", nullable = true, insertable = true, updatable = true, length = 10, precision = 0)
    @Basic
    public int getVintage() {
        return vintage;
    }

    public void setVintage(int vintage) {
        this.vintage = vintage;
    }

    private Producer producer;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.DETACH}, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "producer_id_seq", insertable = true, updatable = true)
    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    private List<Tag> tags;

    @JoinTable(name = "BOTTLE_TAG", joinColumns = @JoinColumn(name = "bottle_id_seq"), inverseJoinColumns = @JoinColumn(name = "tag_id_seq"))
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    private List<Tasting> tastings;

    @OneToMany(mappedBy = "bottle")
    public List<Tasting> getTastings() {
        return tastings;
    }

    public void setTastings(List<Tasting> tastings) {
        this.tastings = tastings;
    }


}
