package info.touret.mwc.model;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * User: touret-a
 * Date: 02/02/12
 * Time: 18:21
 */
public class DateInterval {
    private Date min;
    private Date max;

    public DateInterval(Date min, Date max) {
        this.min = min;
        this.max = max;
    }

    public DateInterval() {
    }

    public Date getMin() {
        return min;
    }

    public void setMin(Date min) {
        this.min = min;
    }

    public Date getMax() {
        return max;
    }

    public void setMax(Date max) {
        this.max = max;
    }

    @Override
    public String toString() {
        return "[" + getMin() + ";" + getMax() + "]";
    }

    public static DateInterval getLastMonths(Integer _months) {
        Date today = new Date();
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.MONTH, -_months);
        Date begin = cal.getTime();
        return new DateInterval(begin, today);
    }

    public static DateInterval getNextMonths(Integer _monthstogo) {
        Date today = new Date();
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.MONTH, _monthstogo);
        Date end = cal.getTime();
        return new DateInterval(today, end);
    }
}
