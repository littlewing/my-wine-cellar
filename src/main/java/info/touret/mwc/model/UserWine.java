package info.touret.mwc.model;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 16/11/11
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
@javax.persistence.Table(name = "user_wine", schema = "public", catalog = "")
@Entity
public class UserWine implements Identifier {
    private int id;

    @javax.persistence.Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "USERWINE_ID_GENERATOR")
    @SequenceGenerator(name = "USERWINE_ID_GENERATOR", sequenceName = "party_id_seq", allocationSize = 10)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    private String token;

    private String username;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserWine(String username) {
        this.username = username;
    }

    public UserWine() {
    }

    @Override
    public String toString() {
        return "UserWine{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
