package info.touret.mwc.model;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 16/11/11
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(schema = "public", name = "tag")
@NamedQueries({@NamedQuery(name = "findByName", query = "select t from Tag t where t.name = :name")})
public class Tag implements Identifier {
    private int id;

    @javax.persistence.Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_ID_GENERATOR")
    @SequenceGenerator(name = "TAG_ID_GENERATOR", sequenceName = "tag_id_seq", allocationSize = 1)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String name;

    @javax.persistence.Column(name = "name", nullable = false, insertable = true, updatable = true, length = 200, precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private UserWine userWine;

    @JoinColumn(name = "user_wine_id", nullable = true, insertable = true, updatable = true)
    @ManyToOne
    public UserWine getUserWine() {
        return userWine;
    }

    public void setUserWine(UserWine userWineId) {
        this.userWine = userWineId;
    }

    private int weight = 1;

    @javax.persistence.Column(name = "weight", nullable = true, insertable = true, updatable = true, length = 10, precision = 0)
    @Basic
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "[" + getName() + "/" + getWeight() + "]";
    }
}
