package info.touret.mwc.service.integration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import info.touret.mwc.model.Producer;
import info.touret.mwc.service.geocoding.GeocoderResponse;
import info.touret.mwc.service.geocoding.MapService;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.FileReader;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * User: touret-a
 * Date: 10/05/12
 * Time: 13:58
 */
@RunWith(Arquillian.class)
public class MapServiceTest extends ArquillianTest {

    @Inject
    MapService mapService;

    Producer producer;

    @Before
    public void setUp() throws Exception {
        producer = new Producer();
        producer.setAddress("22 rue de la montagne");
        producer.setCity("st ouen les vignes");
        producer.setCountry("france");
    }

    public void testGeolocalize() throws Exception {
        producer = mapService.geolocalize(producer);
        assertNotNull(producer);
        assertNotNull(producer.getLattitude());
        assertNotNull(producer.getLongitude());
    }

    @Test
    public void testGetJSONContent() throws Exception {
        Gson gson = new GsonBuilder().create();
        GeocoderResponse response = gson.fromJson(new FileReader("src/test/resources/response.json"), GeocoderResponse.class);
        assertNotNull(response);
        assertNotNull(response.getResults());
        assertTrue(response.getResults().size() > 0);
        assertNotNull(response.getResults().get(0).getGeometry());
        assertNotNull(response.getResults().get(0).getGeometry().getLocation());
        System.out.println("-->" + response.getResults().get(0).getGeometry().getLocation().getLat());
        assertTrue(new Float(47.4672880).equals(response.getResults().get(0).getGeometry().getLocation().getLat()));
    }
}
