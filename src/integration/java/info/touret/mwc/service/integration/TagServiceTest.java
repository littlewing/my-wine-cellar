package info.touret.mwc.service.integration;

import info.touret.mwc.model.Tag;
import info.touret.mwc.service.BottleService;
import info.touret.mwc.service.TagService;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.embeddable.EJBContainer;
import javax.inject.Inject;
import javax.naming.Context;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * User: touret-a
 * Date: 14/12/11
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Arquillian.class)
public class TagServiceTest extends ArquillianTest {
    private static EJBContainer container;
    private static Context context;

    @Inject
    TagService tagService;

    @Inject
    BottleService bottleService;


    @Test
    public void testGetIndexedTags() throws Exception {
        Map<String, Tag> indexedTags = tagService.getIndexedTags();
        assertNotNull(indexedTags);
        Set<String> keys = indexedTags.keySet();
        for (String key : keys) {
            assertNotNull(key);
            assertNotNull(indexedTags.get(key));
            assertNotNull(indexedTags.get(key).getName());
            assertNotNull(indexedTags.get(key).getId());
            System.out.println("weight: [" + indexedTags.get(key).getName() + "---" + indexedTags.get(key).getWeight() + "]");
        }
    }


}
