/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service.integration;

import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.Producer;
import info.touret.mwc.model.Tag;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BottleService;
import info.touret.mwc.service.BusinessService;
import info.touret.mwc.service.ProducerService;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author touret-a
 */
@RunWith(Arquillian.class)
public class BottleServiceTest extends ArquillianTest {

    public BottleServiceTest() {
    }


    @Inject
    private BottleService bottleService;
    @Inject
    private ProducerService producerService;
    @Inject
    private BusinessService<UserWine> userWineBusinessService;

    @Before
    public void setUp() throws Exception {

        bottle = createBottle();
    }

    /**
     * Test of getBottleList method, of class BottleService.
     */
    @Test
    public void testGetBottleList() throws Exception {
        List result = bottleService.getBottleList();
        assertNotNull(result);

    }

    /**
     * Test of findBottlesByTag method, of class BottleService.
     */
    public void testFindBottlesByTag() throws Exception {
        System.out.println("findBottlesByTag");
        Tag tag = null;
        List expResult = null;
        List result = bottleService.findBottlesByTag(tag);
        assertEquals(expResult, result);
    }

    private UserWine user;

    public UserWine getUser() {
        if (user == null) {
            user = new UserWine();
            user.setUsername("johndoe");
            userWineBusinessService.save(user);
        }
        return user;
    }

    public void setUser(UserWine user) {
        this.user = user;
    }

    private Bottle createBottle() {
        Bottle b = new Bottle();
        b.setName("Domaine de la Commanderie");
        b.setQuantity(10);
        b.setTodrink(new Date());
        b.setAcquired(new Date());
        b.setProducer(createProducer());
        b.setUserWine(getUser());
        Tag newTag = new Tag();
        newTag.setName("TAGJUNIT1");
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(newTag);
        newTag.setUserWine(getUser());
        b.setTags(tags);
        return b;
    }

    private Producer createProducer() {
        Producer p = new Producer();
        p.setCountry("FR");
        p.setAddress("HHH");
        p.setName("PrO");
        p.setRegion("CE");
        p.setUserWine(getUser());
        return p;
    }

    @Test
    public void testSave() throws Exception {
        producerService.save(bottle.getProducer());
        bottleService.save(bottle);
        assertNotSame(bottle.getId(), 0);
        assertNotNull(bottle.getProducer());
    }

    @Test
    public void testFindById() throws Exception {
        producerService.save(bottle.getProducer());
        bottleService.save(bottle);
        Bottle b2 = bottleService.findByID(Bottle.class, bottle.getId());
        assertEquals(b2.getId(), bottle.getId());
        assertEquals(b2.getName(), bottle.getName());
        assertNotNull(b2.getProducer());
    }

    @Test
    public void testUpdate() throws Exception {
        Bottle b = createBottle();
    }

    private Bottle bottle;
}
