package info.touret.mwc.service.integration;

import info.touret.mwc.service.calendar.Callback;
import info.touret.mwc.service.calendar.Local;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.util.logging.Logger;

/**
 * User: touret-a
 * Date: 11/04/12
 * Time: 14:07
 */
@RunWith(Arquillian.class)
public class GoogleOAuthTest extends ArquillianTest {
//    @Inject
//    private GoogleOAuth googleOAuth;

    @Inject
    private Logger trace;

    @Inject
    @Local
    private Callback callback;


    @Before
    public void setUp() throws Exception {
//        assertNotNull(googleOAuth);
    }

    @Test
    public void testAuthorizeCalendar() throws Exception {
        //String authorization = googleOAuth.authorizeCalendar();
//        trace.info(authorization);
//        assertNotNull(authorization);
    }

    @Produces
    @Local
    public Callback getCallBack() {
        return new Callback().setUrl("http://blog.touret.info");
    }
}
