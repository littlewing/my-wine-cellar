/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service.integration;


import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;
import info.touret.mwc.service.geocoding.MapService;
import info.touret.mwc.util.Logged;
import info.touret.mwc.util.LoggerFactory;
import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.ResolutionException;
import org.junit.BeforeClass;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.PasswordAuthentication;

import static org.junit.Assert.assertNotNull;

/**
 * @author touret-a
 */
public class ArquillianTest {

    private static final Logger LOGGER = Logger.getLogger(ArquillianTest.class);

    @Deployment
    public static WebArchive createArchive() throws IllegalArgumentException, IOException {

        try {
            setupProxy();
            WebArchive archive = ShrinkWrap.create(WebArchive.class, "my-winecellar-web.war").addAsWebInfResource("WEB-INF/beans.xml",
                    "beans.xml")
                    .addAsResource("logging.properties", "logging.properties")
                    .addAsResource("log4j.properties", "log4j.properties")
                    .addAsResource("oauth_consumer.properties", "oauth_consumer.properties")
                    .addAsResource("socialauth.properties", "socialauth.properties")
                    .addAsResource("client_secrets.json", "client_secrets.json")
                    .addAsResource("META-INF/test-persistence.xml",
                            "META-INF/persistence.xml").addAsWebInfResource("test-ds.xml", "test-ds.xml").
                            addPackages(true, LoggerFactory.class.getPackage(), Bottle.class.getPackage(),
                                    BusinessService.class.getPackage(),
                                    MapService.class.getPackage()).
                            addAsLibraries(
                                    new File("target/test-libs/shrinkwrap-resolver-api.jar"),
                                    new File("target/test-libs/gson.jar"),
                                    new File("target/test-libs/gitkit.jar"),
                                    new File("target/test-libs/json.jar"),
                                    new File("target/test-libs/google-oauth-client.jar"),
                                    new File("target/test-libs/google-api-client.jar"),
                                    new File("target/test-libs/google-http-client.jar"),
                                    new File("target/test-libs/google-http-client-jackson.jar"),
                                    new File("target/test-libs/google-collections.jar"),
                                    new File("target/test-libs/commons-codec.jar"),
                                    new File("target/test-libs/google-api-services-calendar.jar"),
                                    new File("target/test-libs/commons-logging.jar"),
                                    new File("target/test-libs/guava.jar"),
                                    new File("target/test-libs/guava-jdk5.jar"),
                                    new File("target/test-libs/jackson-core-asl.jar"),
                                    new File("target/test-libs/log4j.jar"));
            return archive;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw e;
        } catch (ResolutionException e) {
            e.printStackTrace();
            throw e;
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Inject
    BusinessService<UserWine> userWineBusinessService;

    @BeforeClass
    public static void setupProxy() throws Exception {
        try {
            System.err.println("<> <> GESTION PROXY <> <>");

            if (System.getProperty("http.proxy.user") != null) {
                System.err.println("<> <> GESTION USERS <> <>");
                java.net.Authenticator.setDefault(new java.net.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(System.getProperty("http.proxy.user"), System.getProperty("http.proxy.password").toCharArray());
                    }
                });
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Logged
    @Produces
    public UserWine get() {
        UserWine userWine = new UserWine("alexandre@touret.info");
        userWine = userWineBusinessService.findEqualBySelectedAttributes(UserWine.class, userWine, "username").get(0);
        assertNotNull(userWine);
        return userWine;
    }

}
