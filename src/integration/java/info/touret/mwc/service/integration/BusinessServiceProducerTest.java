package info.touret.mwc.service.integration;

import info.touret.mwc.model.Producer;
import info.touret.mwc.service.ProducerService;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by IntelliJ IDEA.
 * User: touret-a
 * Date: 16/11/11
 * Time: 17:58
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Arquillian.class)
public class BusinessServiceProducerTest extends ArquillianTest {

    @Test
    public void testSave() throws Exception {
        Producer producer = new Producer();
        producer.setName("TEST_BusinessServiceProducerTest");
        producerService.save(producer);
        assertNotNull(producer);
        assertTrue(producer.getId() > 0);
    }

    @Test
    public void testGetList() throws Exception {
        List<Producer> list = producerService.list(Producer.class);
        assertNotNull(list);
    }

    @Inject
    ProducerService producerService;


    @Test
    public void testFindById() throws Exception {
        Producer p = producerService.findByID(Producer.class, 130);
        assertNotNull(p);
    }

    @Test
    public void testUpdate() throws Exception {
        Producer producer = new Producer();
        producer.setName("TEST");
        producerService.save(producer);
        producer.setName("TESTMODIFIED");
        producer.setAddress("TESTMODIFIED");
        producer.setCity("CITYMODIFIED");
        producer.setCountry("FR");
        Producer p2 = new Producer();
        p2 = producerService.update(producer);
        assertEquals(p2.getName(), producer.getName());
        assertEquals(p2.getAddress(), producer.getAddress());
    }

    @Test
    public void testRemove() throws Exception {
        Producer producer = new Producer();
        producer.setName("TEST");
        producerService.save(producer);
        Integer id = producer.getId();
        assertNotNull(id);
        producerService.delete(producer);
        Producer pNotFound = producerService.findByID(Producer.class, id);
        assertNull(pNotFound);
    }

    @Test
    public void testFindLikeBySelectedAttributes() throws Exception {
        Producer producer = new Producer();
        producer.setName("TEST");
        producerService.save(producer);
        System.out.println("lancement du find by selected attrs");
        List<Producer> producers = producerService.findLikeBySelectedAttributes(Producer.class, producer, "name");
        assertNotNull(producers);
        assertTrue(producers.size() > 0);
        System.err.println("producers.get(0).getName()=" + producers.get(0).getName());
        System.err.println("producer.getName()=" + producer.getName());
        assertTrue(producers.get(0).getName().contains(producer.getName()));
    }

    @Test
    public void testFindFullText() throws Exception {
        Producer producer = new Producer();
        producer.setName("TEST");
        producerService.save(producer);
        System.out.println("lancement du find full text");
        List<Producer> producers = producerService.findLikeFullText(Producer.class, producer, "TES", "name");
        assertNotNull(producers);
        assertTrue(producers.size() > 0);
        trace.info("producers.get(0).getName()=" + producers.get(0).getName());
        trace.info("producer.getName()=" + producer.getName());
        assertTrue(producers.get(0).getName().contains(producer.getName()));

    }

    private static final Logger trace = Logger.getAnonymousLogger();
}
