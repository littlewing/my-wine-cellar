package info.touret.mwc.service.integration;

import info.touret.mwc.model.Bottle;
import info.touret.mwc.service.ReminderService;
import info.touret.mwc.service.calendar.CalendarServiceFactory;
import info.touret.mwc.service.calendar.Local;
import org.apache.log4j.Logger;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;

/**
 * User: touret-a
 * Date: 02/04/12
 * Time: 17:01
 */
@RunWith(Arquillian.class)
public class ReminderTest extends ArquillianTest {

    private static final Logger LOGGER = Logger.getLogger(ReminderTest.class);
    @Inject
    ReminderService reminderService;

    public CalendarServiceFactory getCalendarServiceFactory() {
        return calendarServiceFactory;
    }


    public void setCalendarServiceFactory(@Local CalendarServiceFactory _calendarServiceFactory) {
        this.calendarServiceFactory = _calendarServiceFactory;
    }

    CalendarServiceFactory calendarServiceFactory;


    @Before
    public void setUp() throws Exception {

        setCalendarServiceFactory(new CalendarServiceFactory());
    }

    @Test
    public void testCreateEvent() throws Exception {
        LOGGER.info("creation data");
        Bottle bottle = new Bottle();
        bottle.setTodrink(new Date());
        LOGGER.info("creation de l evenement...");
        reminderService.createEvent(bottle);
        LOGGER.info("evenenment cree...");
    }


}
