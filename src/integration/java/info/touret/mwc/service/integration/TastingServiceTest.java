package info.touret.mwc.service.integration;

import info.touret.mwc.model.Bottle;
import info.touret.mwc.model.DateInterval;
import info.touret.mwc.model.Producer;
import info.touret.mwc.model.Tasting;
import info.touret.mwc.service.BottleService;
import info.touret.mwc.service.TastingService;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: touret-a
 * Date: 31/01/12
 * Time: 09:52
 */
@RunWith(Arquillian.class)
public class TastingServiceTest extends ArquillianTest {

    @Inject
    private BottleService bottleService;
    @Inject
    private TastingService tastingService;


    private Tasting createTasting() {
        Producer producer = new Producer();
        producer.setName("PRODUCER");
        Bottle bottle = new Bottle();
        bottle.setName("BOTTLE1");
        bottle.setAcquired(new Date());
        bottle.setQuantity(12);
        bottle.setProducer(producer);
        bottle.setTodrink(new Date());
        Tasting tasting = new Tasting();
        tasting.setName("TASTING");
        tasting.setBottle(bottle);
        tasting.setQuantity(2);
        tasting.setObservation("....");
        tasting.setRate(6D);
        tasting.setTastingdate(new Date());
        return tasting;
    }

    @Test
    public void testSaveNew() throws Exception {
        Tasting tasting = createTasting();
        tastingService.save(tasting);
        assertEquals(tasting.getBottle().getQuantity(), 10);
    }

    @Test
    public void testFindLastTastings() throws Exception {
        List<Tasting> tastings = tastingService.getLastTastings(DateInterval.getLastMonths(10));
        assertNotNull(tastings);
        assertTrue(tastings.size() > 0);

    }

    @Test
    public void testFindBestTastings() throws Exception {
        List<Tasting> tastings = tastingService.getBestTastings(10);
        assertNotNull(tastings);
        assertTrue(tastings.size() > 0);
        assertTrue(tastings.size() <= 10);
    }
}
