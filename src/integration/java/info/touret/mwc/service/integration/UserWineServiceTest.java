/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.touret.mwc.service.integration;

import info.touret.mwc.model.UserWine;
import info.touret.mwc.service.BusinessService;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * @author touret-a
 */
@RunWith(Arquillian.class)
public class UserWineServiceTest extends ArquillianTest {

    public UserWineServiceTest() {
    }


    @Inject
    private BusinessService<UserWine> userWineBusinessService;


    @Before
    public void setUp() throws Exception {
    }


    private UserWine user;

    public UserWine getUser() {
        if (user == null) {
            user = new UserWine();
            user.setUsername("johndoe");
            userWineBusinessService.save(user);
        }
        return user;
    }

    public void setUser(UserWine user) {
        this.user = user;
    }


    @Test
    public void testSave() throws Exception {
        userWineBusinessService.save(getUser());
        assertNotNull(getUser());
        assertNotSame(getUser().getId(), 0);
    }

    @Test
    public void testFindById() throws Exception {
        assertNotNull(userWineBusinessService.findByID(UserWine.class, 1));
    }

    @Test
    public void testUpdate() throws Exception {

    }

    @Test
    public void testEqualBySelectedAttributes() {
        List<UserWine> userWines = userWineBusinessService.findEqualBySelectedAttributes(UserWine.class, getUser(), "username");
        assertNotNull(userWines);
        for (UserWine userWine : userWines) {
            assertEquals("test de l egalite des valeurs retournees:", user.getUsername(), userWine.getUsername());
        }
        assertTrue(userWines.size() > 0);
    }

    private static Logger logger = Logger.getLogger(UserWineServiceTest.class.getCanonicalName());
}
